package com.mariovaldez.primetimetest.data.remote.response

import com.mariovaldez.primetimetest.data.model.Thumbnail

data class ComicResult(
    val id : Int,
    val digitalId: Int,
    val title: String,
    val issueNumber: Int,
    val variantDescription: String,
    val description: String,
    val modified : String,
    val isbn : String,
    val upc : String,
    val diamondCode : String,
    val resourceURI: String,
    val thumbnail: Thumbnail
)