package com.mariovaldez.primetimetest.data.remote.response

import com.mariovaldez.primetimetest.data.model.Thumbnail

data class SerieResult(
    val id : Int,
    val title: String,
    val variantDescription: String,
    val description: String,
    val modified : String,
    val resourceURI: String,
    val thumbnail: Thumbnail
)
