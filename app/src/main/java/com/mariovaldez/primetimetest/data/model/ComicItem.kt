package com.mariovaldez.primetimetest.data.model

data class ComicItem(
    private val available: Int,
    private val collectionURI: String,
    private val items: List<Item>
)
