package com.mariovaldez.primetimetest.data.model

data class Thumbnail(
    val path:String,
    val extension:String,
    val type: String,
    val author : String
)
