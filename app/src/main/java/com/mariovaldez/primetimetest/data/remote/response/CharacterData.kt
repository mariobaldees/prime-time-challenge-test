package com.mariovaldez.primetimetest.data.remote.response

data class CharacterData(
    val offset : Int,
    val limit: Int,
    val total: Int,
    val count: Int,
    val results: List<CharacterResult>
)
