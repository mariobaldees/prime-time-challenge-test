package com.mariovaldez.primetimetest.data.remote.response

import com.mariovaldez.primetimetest.data.model.Thumbnail

data class CharacterResult(
    val id: String,
    val name: String,
    val description: String,
    val modified: String ,
    val thumbnail: Thumbnail,
    val resourceURI: String,
)
