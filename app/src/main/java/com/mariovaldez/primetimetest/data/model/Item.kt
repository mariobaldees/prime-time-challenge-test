package com.mariovaldez.primetimetest.data.model

data class Item(
    private val resourceURI:String,
    private val name: String
)
