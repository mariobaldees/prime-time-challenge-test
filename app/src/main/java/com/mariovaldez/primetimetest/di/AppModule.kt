package com.mariovaldez.primetimetest.di

import com.google.gson.GsonBuilder
import com.mariovaldez.primetimetest.network.remote.MarvelAPI
import com.mariovaldez.primetimetest.other.Constants
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun providesMarvelAPI(retrofit: Retrofit):MarvelAPI{
        return retrofit.create(MarvelAPI::class.java)
    }

    @Provides
    @Singleton
    fun providesRetrofit() : Retrofit{
        val gsonBuilder = GsonBuilder()
        gsonBuilder.serializeNulls()
        val gson = gsonBuilder.create()
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(gson))
            .baseUrl(Constants.BASE_URL)
            .build()
    }
}