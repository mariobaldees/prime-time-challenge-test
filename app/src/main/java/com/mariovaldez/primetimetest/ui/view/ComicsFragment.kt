package com.mariovaldez.primetimetest.ui.view

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.mariovaldez.primetimetest.adapter.CharacterAdapter
import com.mariovaldez.primetimetest.adapter.ComicAdapter
import com.mariovaldez.primetimetest.adapter.SeriesAdapter
import com.mariovaldez.primetimetest.data.remote.response.CharacterResult
import com.mariovaldez.primetimetest.data.remote.response.ComicResult
import com.mariovaldez.primetimetest.databinding.FragmentComicsBinding
import com.mariovaldez.primetimetest.ui.viewmodel.ComicViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ComicsFragment: Fragment() {

    lateinit var binding: FragmentComicsBinding
    private lateinit var adapterComics:ComicAdapter
    private lateinit var adapterSeries:SeriesAdapter
    private lateinit var adapterCharacter:CharacterAdapter
    lateinit var comicResult: List<ComicResult>
    lateinit var characterResult: List<CharacterResult>

    private val viewmodel: ComicViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentComicsBinding.inflate(inflater,container,false)
        start()
        return binding.root
    }

    private fun start() {
        setupAdapters()
        setupListeners()
        if(viewmodel.checkIntenetConnection(requireContext())){
            viewmodel.getData()
        }else{
            showAlert("No estás conectado a Internet, favor de conectarse para poder utilizar la app.")
            val timer = object: CountDownTimer(5000, 1000) {
                override fun onTick(millisUntilFinished: Long) {}
                override fun onFinish() {   exit()  }
            }
            timer.start()
        }

    }


    fun exit(){
       requireActivity().finish()
    }

    private fun setupListeners() {

        //Characters Result Api observer
        viewmodel.characterResult.observe(viewLifecycleOwner,{
            adapterCharacter.characterItems = it
        })

        //Comics Result Api observer
        viewmodel.comicResult.observe(viewLifecycleOwner,{
            adapterComics.comicItems = it
        })

        //Series Result Api observer
        viewmodel.serieResult.observe(viewLifecycleOwner,{
            adapterSeries.serieItems = it
            binding.cardLoading.visibility = View.GONE
        })
    }

    private fun setupAdapters() {

        //Comics Adapter Init
        adapterComics = ComicAdapter()
        binding.rvComics.apply {
            adapter = adapterComics
            adapter!!.notifyDataSetChanged()
            layoutManager = LinearLayoutManager(requireActivity(),LinearLayoutManager.HORIZONTAL,false)
            setHasFixedSize(true)
        }

        //Characters Adapter Init
        adapterCharacter = CharacterAdapter()
        binding.rvCharacters.apply {
            adapter = adapterCharacter
            adapter!!.notifyDataSetChanged()
            layoutManager = LinearLayoutManager(requireActivity(),LinearLayoutManager.HORIZONTAL,false)
            setHasFixedSize(true)
        }

        //Characters Adapter Init
        adapterSeries = SeriesAdapter()
        binding.rvSeries.apply {
            adapter = adapterSeries
            adapter!!.notifyDataSetChanged()
            layoutManager = LinearLayoutManager(requireActivity(),LinearLayoutManager.HORIZONTAL,false)
            setHasFixedSize(true)
        }
        adapterCharacter.context = requireContext()
        adapterComics.context = requireContext()
        adapterSeries.context = requireContext()

    }
    fun showAlert(message:String){
        val dlgAlert: android.app.AlertDialog.Builder = android.app.AlertDialog.Builder(requireContext())
        dlgAlert.setTitle("Intente de nuevo")
        dlgAlert.setMessage(message)
        dlgAlert.setCancelable(true)
        val alertDialog = dlgAlert.create()
        alertDialog.show()
        val timer = object: CountDownTimer(4000, 1000) {
            override fun onTick(millisUntilFinished: Long) {}
            override fun onFinish() {alertDialog.cancel()}
        }
        timer.start()
    }

}