package com.mariovaldez.primetimetest.ui.viewmodel

import android.content.Context
import android.net.ConnectivityManager
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mariovaldez.primetimetest.data.remote.response.*
import com.mariovaldez.primetimetest.repositories.ComicRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class ComicViewModel @Inject constructor(
    private val repository: ComicRepository
):ViewModel(){

    private val _comicResult= MutableLiveData<List<ComicResult>>()
    val comicResult: LiveData<List<ComicResult>>
        get() = _comicResult
    private val _characterResult= MutableLiveData<List<CharacterResult>>()
    val characterResult: LiveData<List<CharacterResult>>
        get() = _characterResult
    private val _serieResult= MutableLiveData<List<SerieResult>>()
    val serieResult: LiveData<List<SerieResult>>
        get() = _serieResult

    private fun getCharacters(){
        repository.getCharacters().enqueue(object : Callback<CharacterResponse> {
            override fun onResponse(
                call: Call<CharacterResponse>,
                response: Response<CharacterResponse>
            ) {
                if(response.isSuccessful){
                    println(response.body())
                    _characterResult.postValue(response.body()!!.data.results)
                }else{
                    println(response.message())
                }

            }

            override fun onFailure(call: Call<CharacterResponse>, t: Throwable) {
                println(t.message)
            }

        })
    }

    private fun getComics(){
        repository.getComics().enqueue(object: Callback<ComicResponse>{
            override fun onResponse(call: Call<ComicResponse>, response: Response<ComicResponse>) {
                if(response.isSuccessful){
                    println(response.body())
                    _comicResult.postValue(response.body()!!.data.results)
                }else{
                    println(response.message())
                }
            }

            override fun onFailure(call: Call<ComicResponse>, t: Throwable) {
                println(t.message)
            }

        })
    }
    private fun getSeries(){
        repository.getSeries().enqueue(object : Callback<SeriesReponse>{
            override fun onResponse(call: Call<SeriesReponse>, response: Response<SeriesReponse>) {
                if(response.isSuccessful){
                    println(response.body())
                    _serieResult.postValue(response.body()!!.data.results)
                }else{
                    println(response.message())
                }
            }

            override fun onFailure(call: Call<SeriesReponse>, t: Throwable) {
                println(t.message)
            }

        })
    }

    fun getData(){
        viewModelScope.launch {
            getComics()
            getCharacters()
            getSeries()
        }
    }

    fun checkIntenetConnection(context: Context?) : Boolean{
        val cm = context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        return cm.activeNetworkInfo != null && cm.activeNetworkInfo!!.isConnected
    }
}