package com.mariovaldez.primetimetest.repositories

import com.mariovaldez.primetimetest.network.remote.MarvelAPI
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ComicRepository @Inject constructor(
    private val api : MarvelAPI
) {

    fun getCharacters() = api.getCharacters()
    fun getComics() = api.getComics()
    fun getSeries() = api.getSeries()
}