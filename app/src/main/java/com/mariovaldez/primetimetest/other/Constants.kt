package com.mariovaldez.primetimetest.other

object Constants {
    const val BASE_URL = "https://gateway.marvel.com:443/v1/public/"
}