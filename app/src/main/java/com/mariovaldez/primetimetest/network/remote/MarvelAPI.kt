package com.mariovaldez.primetimetest.network.remote


import com.mariovaldez.primetimetest.BuildConfig
import com.mariovaldez.primetimetest.data.remote.response.CharacterResponse
import com.mariovaldez.primetimetest.data.remote.response.ComicResponse
import com.mariovaldez.primetimetest.data.remote.response.SeriesReponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface MarvelAPI {
    @GET("comics")
    fun getComics(
        @Query("hash") hash:String = BuildConfig.HASH,
        @Query("apikey")apikey:String = BuildConfig.API_KEY,
        @Query("ts")ts:String = "1"
        ):Call<ComicResponse>

    @GET("characters")
    fun getCharacters(
        @Query("hash") hash:String = BuildConfig.HASH,
        @Query("apikey")apikey:String = BuildConfig.API_KEY,
        @Query("ts")ts:String = "1"
        ):Call<CharacterResponse>

    @GET("series")
    fun getSeries(
        @Query("hash") hash:String = BuildConfig.HASH,
        @Query("apikey")apikey:String = BuildConfig.API_KEY,
        @Query("ts")ts:String = "1"
        ):Call<SeriesReponse>
}