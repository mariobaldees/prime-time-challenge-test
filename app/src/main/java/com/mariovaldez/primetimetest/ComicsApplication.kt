package com.mariovaldez.primetimetest

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class ComicsApplication : Application() {}
