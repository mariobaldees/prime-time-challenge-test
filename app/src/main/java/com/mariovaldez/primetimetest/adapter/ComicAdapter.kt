package com.mariovaldez.primetimetest.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.mariovaldez.primetimetest.data.remote.response.ComicResult
import com.mariovaldez.primetimetest.databinding.ItemComicBinding

class ComicAdapter: RecyclerView.Adapter<ComicAdapter.ComicViewHolder>() {
    inner class ComicViewHolder(val binding: ItemComicBinding): RecyclerView.ViewHolder(binding.root) {}

    private val diffCallback = object  : DiffUtil.ItemCallback<ComicResult>(){
        override fun areItemsTheSame(oldItem: ComicResult, newItem: ComicResult): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: ComicResult, newItem: ComicResult): Boolean {
            return oldItem == newItem
        }

    }

    private val differ = AsyncListDiffer(this,diffCallback)
    lateinit var context: Context
    var comicItems: List<ComicResult>
        get() = differ.currentList
        set(value) {
            differ.submitList(value)
        }

    override fun onBindViewHolder(holder: ComicViewHolder, position: Int) {
        val currentComic = comicItems[position]
        holder.setIsRecyclable(false);
        holder.binding.apply {
            txtComicTitle.text = currentComic.title
            val url = currentComic.thumbnail.path +"." + currentComic.thumbnail.extension
            Glide.with(context)
                .load(url)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .dontAnimate()
                .into(imgComic)
        }
    }

    override fun getItemCount(): Int {
        return comicItems.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ComicViewHolder {
        context = parent.context
        val view = ItemComicBinding.inflate(LayoutInflater.from(context),parent,false)
        return ComicViewHolder(view)
    }
}