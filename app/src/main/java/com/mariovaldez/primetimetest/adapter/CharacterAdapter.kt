package com.mariovaldez.primetimetest.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.mariovaldez.primetimetest.data.remote.response.CharacterResult
import com.mariovaldez.primetimetest.databinding.ItemCharacterBinding
import com.bumptech.glide.request.RequestOptions
import com.mariovaldez.primetimetest.R


class CharacterAdapter: RecyclerView.Adapter<CharacterAdapter.CharacterViewHolder>() {
    inner class CharacterViewHolder(val binding: ItemCharacterBinding):RecyclerView.ViewHolder(binding.root) {}

    private val diffCallback = object  : DiffUtil.ItemCallback<CharacterResult>(){
        override fun areItemsTheSame(oldItem: CharacterResult, newItem: CharacterResult): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: CharacterResult, newItem: CharacterResult): Boolean {
            return oldItem == newItem
        }

    }

    private val differ = AsyncListDiffer(this,diffCallback)
    lateinit var context: Context
    var characterItems: List<CharacterResult>
        get() = differ.currentList
        set(value) {
            differ.submitList(value)
        }

    override fun onBindViewHolder(holder: CharacterViewHolder, position: Int) {
        val currentCharacter = characterItems[position]
        holder.setIsRecyclable(false);
        holder.binding.apply {
            val url = currentCharacter.thumbnail.path +"." + currentCharacter.thumbnail.extension
                Glide.with(context)
                    .load(url)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .dontAnimate()
                    .error(R.drawable.ic_launcher_background)
                    .circleCrop()
                    .into(imgCharacter)
            characterName.text = currentCharacter.name
        }
    }

    override fun getItemCount(): Int {
        return characterItems.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CharacterViewHolder {
        context = parent.context
        val view = ItemCharacterBinding.inflate(LayoutInflater.from(context),parent,false)
        return CharacterViewHolder(view)
    }
}