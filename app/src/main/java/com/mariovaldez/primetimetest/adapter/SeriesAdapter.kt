package com.mariovaldez.primetimetest.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.mariovaldez.primetimetest.data.remote.response.ComicResult
import com.mariovaldez.primetimetest.data.remote.response.SerieResult
import com.mariovaldez.primetimetest.databinding.ItemSerieBinding

class SeriesAdapter : RecyclerView.Adapter<SeriesAdapter.SeriesViewHolder>() {

    inner class SeriesViewHolder(val binding: ItemSerieBinding):RecyclerView.ViewHolder(binding.root)

    private val diffCallback = object  : DiffUtil.ItemCallback<SerieResult>(){
        override fun areItemsTheSame(oldItem: SerieResult, newItem: SerieResult): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: SerieResult, newItem: SerieResult): Boolean {
            return oldItem == newItem
        }

    }

    private val differ = AsyncListDiffer(this,diffCallback)
    lateinit var context: Context
    var serieItems: List<SerieResult>
        get() = differ.currentList
        set(value) {
            differ.submitList(value)
        }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SeriesViewHolder {
        context = parent.context
        val view = ItemSerieBinding.inflate(LayoutInflater.from(context),parent,false)
        return SeriesViewHolder(view)
    }

    override fun onBindViewHolder(holder: SeriesViewHolder, position: Int) {
        val currentSerie = serieItems[position]

        holder.binding.apply {
            txtSerieTitle.text = currentSerie.title
            val url = currentSerie.thumbnail.path +"." + currentSerie.thumbnail.extension
            Glide.with(context)
                .load(url)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .dontAnimate()
                .into(imgSerie)
        }
    }

    override fun getItemCount(): Int {
        return serieItems.size
    }

}