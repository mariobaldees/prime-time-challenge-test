package com.mariovaldez.primetimetest.di

import com.google.common.truth.Truth
import com.mariovaldez.primetimetest.network.remote.MarvelAPI
import com.mariovaldez.primetimetest.other.Constants
import org.junit.Before
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class AppModuleTest {
    private lateinit var api :MarvelAPI
    @Before
    fun setup(){
        api = Rest.create(Constants.BASE_URL)
    }
    // TEST GET CHARACTERS CALL
    @Test
    fun get_characters_code_response_equals_200_returns_true() {
        val response = api.getCharacters().execute()
        Truth.assertThat(response.code()).isEqualTo(200)
    }

    @Test
    fun get_characters_code_response_not_equals_200_returns_true() {
        val response = api.getCharacters().execute()
        Truth.assertThat(response.code()).isNotEqualTo(200)
    }

    @Test
    fun get_characters_body_response_not_null_returns_true() {
        val response = api.getCharacters().execute()
        Truth.assertThat(response.body()).isNotNull()
    }

    @Test
    fun get_characters_body_response_null_returns_false() {
        val response = api.getCharacters().execute()
        Truth.assertThat(response.body()).isNull()
    }

    // TEST GET COMICS CALL

    @Test
    fun get_comics_code_response_equals_200_returns_true() {
        val response = api.getComics().execute()
        Truth.assertThat(response.code()).isEqualTo(200)
    }

    @Test
    fun get_comics_code_response_not_equals_200_returns_true() {
        val response = api.getComics().execute()
        Truth.assertThat(response.code()).isNotEqualTo(200)
    }

    @Test
    fun get_comics_body_response_not_null_returns_true() {
        val response = api.getComics().execute()
        Truth.assertThat(response.body()).isNotNull()
    }

    @Test
    fun get_comics_body_response_null_returns_false() {
        val response = api.getComics().execute()
        Truth.assertThat(response.body()).isNull()
    }

    // TEST GET SERIES CALL

    @Test
    fun get_series_code_response_equals_200_returns_true() {
        val response = api.getSeries().execute()
        Truth.assertThat(response.code()).isEqualTo(200)
    }

    @Test
    fun get_series_code_response_not_equals_200_returns_true() {
        val response = api.getSeries().execute()
        Truth.assertThat(response.code()).isNotEqualTo(200)
    }

    @Test
    fun get_series_body_response_not_null_returns_true() {
        val response = api.getSeries().execute()
        Truth.assertThat(response.body()).isNotNull()
    }

    @Test
    fun get_series_body_response_null_returns_false() {
        val response = api.getSeries().execute()
        Truth.assertThat(response.body()).isNull()
    }

}

interface Rest{
    companion object{
        fun create(base_url : String): MarvelAPI{
            val retrofit = Retrofit.Builder()
                .baseUrl(base_url)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
            return retrofit.create(MarvelAPI::class.java)
        }
    }
}